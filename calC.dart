import 'dart:io';
import 'dart:math';

class CalOOP {
  String operation = "";
  var num1 = 0;
  var num2 = 0;
  var geo = 0;

  showAll() {
    print("Welcome to Calculator, please select the operator.");
    print("1.Add (+)");
    print("2.Substract (-)");
    print("3.Multiply (*)");
    print("4.Divide (/)");
    print("5.Power (^)");
    print("6.Factorail (!)");
    print("7.Calculate Geometry (g)");
    print("8.exit (e)");
    operatorCal(operation);
  }

  operatorCal(String operation) {
    while (true) {
      print("Input your operator in program.");
      operation = stdin.readLineSync()!;
      if (operation == "e") {
        print("-------------------------");
        print("Thanks for using, see you again next time :-)");
        print("Bye Bye~");
        break;
      }
      if (operation == "+") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        print("Input your second number in program");
        num2 = int.parse(stdin.readLineSync()!);
        int sum = num1 + num2;
        print("Sum of the add numbers is : $sum");
      } else if (operation == "-") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        print("Input your second number in program");
        num2 = int.parse(stdin.readLineSync()!);
        int sum = num1 - num2;
        print("Sum of the substract numbers is : $sum");
      } else if (operation == "*") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        print("Input your second number in program");
        num2 = int.parse(stdin.readLineSync()!);
        int sum = num1 * num2;
        print("Sum of the multiply numbers is : $sum");
      } else if (operation == "/") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        print("Input your second number in program");
        num2 = int.parse(stdin.readLineSync()!);
        double a1 = num1 / num2;
        print("Sum of the divide numbers is : $a1");
      } else if (operation == "^") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        print("Input your second number in program");
        num2 = int.parse(stdin.readLineSync()!);
        stdout.write("Sum of the pow numbers is : ");
        stdout.write(pow(num1, num2));
        print("");
      } else if (operation == "!") {
        print("Input your first number in program");
        num1 = int.parse(stdin.readLineSync()!);
        int fac = 1;
        for (int i = 1; i <= num1; i++) {
          fac *= i;
        }
        print("Sum of the factorial numbers is : $fac");
      } else if (operation == "g") {
        showMenuGeo();
        geometryCal(geo);
        break;
      } else {
        print("Please try again to input");
        operatorCal(operation);
        break;
      }
    }
  }

  showMenuGeo() {
    print("-------------------------");
    print("Welcome to the Calculator of Geometry, please input your number.");
    print(" - Triangle (1)");
    print(" - Equilateral Triangle (2)");
    print(" - Radius (3)");
    print(" - Square (4)");
    print(" - exit the program (0)");
  }

  geometryCal(int geo) {
    double area = 0.0;
    while (true) {
      print("Input your geometry numbers in program.");
      geo = int.parse(stdin.readLineSync()!);
      if (geo == 0) {
        print("-------------------------");
        showAll();
        break;
      }
      if (geo == 1) {
        print("Input the base");
        double base = double.parse(stdin.readLineSync()!);
        print("Input the height");
        double height = double.parse(stdin.readLineSync()!);
        area = 0.5 * base * height;
        print("The Triangle Area is : $area");
      } else if (geo == 2) {
        print("Input the Equilateral");
        double equal = double.parse(stdin.readLineSync()!);
        area = (sqrt(3) / 4) * pow(equal, 2);
        area = double.parse((area).toStringAsFixed(2));
        print("The Equilateral Triangle Area is : $area");
      } else if (geo == 3) {
        print("Input the Radius");
        double radius = double.parse(stdin.readLineSync()!);
        area = pi * (pow(radius, radius));
        area = double.parse((area).toStringAsFixed(2));
        print("The Radius Area is : $area");
      } else if (geo == 4) {
        print("Input the Side of Square number");
        double side = double.parse(stdin.readLineSync()!);
        area = side * side;
        print("The Side of Square Area is : $area");
      } else {
        print("Out of choice, please try again to input your number.");
        geometryCal(geo);
        break;
      }
    }
  }
}
